package main

import (
	"math"
	"encoding/json"
	"strconv"
	"time"
	"github.com/rs/zerolog/log"
	"net/http"
	"os"
	"html/template"
	"fmt"
	"net/url"
	"flag"
)

var tpl = template.Must(template.ParseFiles("index.html"))

var apiKey *string

//Source struct is separeated from NewsAPi
type Source struct {
	ID interface{} `json:"id"`
	Name string `json:"name"`
}

//Article struct created for NEwsAPi
type Article struct {
		Source Source `json:"source"`
		Author      string    `json:"author"`
		Title       string    `json:"title"`
		Description string    `json:"description"`
		URL         string    `json:"url"`
		URLToImage  string    `json:"urlToImage"`
		PublishedAt time.Time `json:"publishedAt"`
		Content     string    `json:"content"`
}

//FormatPublishedDate format the date string 
func (a *Article) FormatPublishedDate() string {
	year, month, day := a.PublishedAt.Date()
	return fmt.Sprintf("%v %d %d", month, day, year)
}

//Results struct created from NewsAPI
type Results struct {
	Status string `json:"status"`
	TotalResults int    `json:"totalResults"`
	Articles []Article `json:"articles"`
}

//Search struct is for search query
type Search struct {
	SearchKey string
	NextPage int
	TotalPages int
	Results Results
}

//IsLastPage check whether it is a last page or not
func (s *Search) IsLastPage() bool {
	return s.NextPage >= s.TotalPages
}

//CurrentPage helps to decremnt the page
func (s *Search) CurrentPage() int {
	if s.NextPage == 1 {
		return s.NextPage
	}

	return s.NextPage - 1
}

//PreviousPage decrement the page
func (s *Search) PreviousPage() int {
	return s.CurrentPage() - 1
}

func indexHandler(w http.ResponseWriter, r *http.Request) {
	tpl.Execute(w, nil)
}

//NewsAPIError returns correct error messages
type NewsAPIError struct {
	Status string `json:"status"`
	Code string `json:"Code"`
	Message string `json:"message"`
}

func searchHandler(w http.ResponseWriter, r *http.Request) {
	u, err := url.Parse(r.URL.String())
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Internal Server Error!"))
		return
	}

	params := u.Query()
	searchKey := params.Get("q")
	page := params.Get("page")
	if page == "" {
		page = "1"
	}

	search := &Search{}
	search.SearchKey = searchKey

	next, err := strconv.Atoi(page)
	if err != nil {
		http.Error(w, "Unexpected server error", http.StatusInternalServerError)
		return
	}

	search.NextPage = next
	pageSize := 20

	endPoint := fmt.Sprintf("https://newsapi.org/v2/everything?q=%s&pageSize=%d&page=%d&apiKey=%s&sortBy=publishedAt&language=en", url.QueryEscape(search.SearchKey),pageSize,search.NextPage,*apiKey)
	resp, err := http.Get(endPoint)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	defer resp.Body.Close()

	if resp.StatusCode != 200 {
		newError := &NewsAPIError{}
		err := json.NewDecoder(resp.Body).Decode(newError)
		if err != nil {
			http.Error(w, "Unexpected Server Error", http.StatusInternalServerError)
			return
		}

		http.Error(w, newError.Message, http.StatusInternalServerError)
		return 
	}

	err = json.NewDecoder(resp.Body).Decode(&search.Results)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	search.TotalPages = int(math.Ceil(float64(search.Results.TotalResults / pageSize)))
	err = tpl.Execute(w, search)

	if ok := !search.IsLastPage(); ok {
		search.NextPage++
	}

	if err != nil {
		log.Printf(err.Error())
	}

	fmt.Println("Search Query is: ", searchKey)
	fmt.Println("Result Page is: ", page)
}

func main() {
	apiKey = flag.String("apikey", "7e0f88ac84f24965aa6efa7323e1fa9f", "Newsapi.org access key")
	flag.Parse()

	if *apiKey == "" {
		log.Fatal().Msg("apikey must be set")
	}

	port := os.Getenv("PORT")
	if port == "" {
		port = "3000"
	}

	mux := http.NewServeMux()

	fs := http.FileServer(http.Dir("assets"))
	mux.Handle("/assets/", http.StripPrefix("/assets/", fs))

	mux.HandleFunc("/", indexHandler)
	mux.HandleFunc("/search", searchHandler)
	http.ListenAndServe(":"+port, mux)
}